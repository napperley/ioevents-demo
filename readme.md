# IO Events Demo (ioevents-demo)

A [Kotlin Native](https://github.com/JetBrains/kotlin-native) program that demonstrates how to handle IO events.
