@file:Suppress("EXPERIMENTAL_UNSIGNED_LITERALS", "EXPERIMENTAL_API_USAGE")

package org.example.ioevents_demo

import kotlinx.cinterop.*
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import platform.linux.*
import platform.posix.*
import kotlin.experimental.or
import kotlin.system.exitProcess

fun main() {
    println("Starting IO Events Demo...")
//    println("Running file watcher example...")
//    doFileWatcher()
//    println("Running poll example...")
//    doPoll()
    println("Running scheduling example")
    doScheduling()
    println("Exiting...")
}

private fun doScheduling() {
    runBlocking {
        launch {
            runPeriodically(2) { println("Two second task") }
        }
        launch {
            runPeriodically(5) { println("Five second task") }
        }
    }
}

private fun doFileWatcher() = memScoped {
    val filePath = "/dev"
    val (notifyFd, _) = watchFile(filePath, FileWatchEvent.CREATE, FileWatchEvent.DELETE)
    val maxEvents = 10
    val bufferSize = maxEvents * (sizeOf<inotify_event>() + PATH_MAX)
    val buffer = allocArray<inotify_event>(bufferSize)
    val fileNameFilter = { fileName: String -> fileName.startsWith("ttyUSB") }

    println("Listening for changes to $filePath...")
    while (true) {
        val totalRead = read(__fd = notifyFd, __buf = buffer, __nbytes = bufferSize.toULong())
        if (totalRead > 0) {
            println("Changes detected in $filePath.")
            println("Listing files in $filePath...")
            val dir = opendir(filePath)
            fileNameList(dir).filter(fileNameFilter).forEach { println("* $it") }
            closedir(dir)
        } else {
            fprintf(stderr, "Encountered error with reading file descriptor.")
            exitProcess(-1)
        }
        sleep(1)
    }
}

/** Watches a file or directory for changes based on events. @see [FileWatchEvent] */
private fun watchFile(filePath: String, vararg events: FileWatchEvent): Pair<Int, Int> {
    val notifyFd = inotify_init()
    val error = -1
    if (notifyFd == error) fprintf(stderr, "Failed to initialize inotify.")
    // Add a file watch and get the watch file descriptor.
    val watchFd = inotify_add_watch(__fd = notifyFd, __name = filePath,
            __mask = createFileWatchEventMask(*events).toUInt())
    if (watchFd == error) fprintf(stderr, "Cannot add watch for $filePath.")
    return notifyFd to watchFd
}

private fun fileNameList(dir: CPointer<DIR>?): Array<String> {
    val tmp = mutableListOf<String>()
    var dirEntry: CPointer<dirent>?
    if (dir != null) {
        do {
            dirEntry = readdir(dir)
            // If dirEntry isn't null add the directory/file name to tmp.
            if (dirEntry != null) tmp += dirEntry.pointed.d_name.toKString()
        } while (dirEntry != null)
        tmp -= "."
        tmp -= ".."
    }
    return tmp.toTypedArray()
}

/** Runs the poll example. Based on the code in the [YouTube video](https://youtu.be/UP6B324Qh5k) */
private fun doPoll() = memScoped {
    // The file descriptor for terminal input.
    val fd = 0
    var pollRc: Int
    val pollFds = allocArray<pollfd>(1)
    // Timeout in ms.
    val timeout = 5000
    val bufferLength = 11uL
    val buffer = allocArray<ByteVar>(bufferLength.toInt())

    while (true) {
        pollFds[0].fd = fd
        // Wish Item: Bit manipulation operators (eg |=).
        pollFds[0].events = pollFds[0].events or POLLIN.toShort()
        pollRc = poll(__fds = pollFds, __nfds = 1, __timeout = timeout)
        processPollResult(pollRc = pollRc, fd = fd, buffer = buffer, bufferLength = bufferLength)
        // Prevent high CPU usage from occurring.
        sleep(1)
    }
}

private fun processPollResult(pollRc: Int, fd: Int, buffer: CArrayPointer<ByteVar>, bufferLength: ULong) = memScoped {
    val empty = -1L
    val timeout = 0
    if (pollRc == timeout) {
        println("Poll Result Code: $pollRc")
        println("Timeout\n")
    } else {
        // Clear the buffer.
        memset(buffer, 0, bufferLength)
        val readRc = read(__fd = fd, __buf = buffer, __nbytes = bufferLength - 1uL)
        println("Read Result Code: $readRc")
        if (readRc != empty) {
            println("Buffer: ${buffer.toKString().replace("\n", "")}")
        }
    }
}

/**
 * Runs a [task] periodically.
 * @param num A whole number to use for scheduling the running of the task.
 * @param timeUnit The [unit][TimeUnit] of time to use for scheduling.
 * @param task The function to use which will do things in the background.
 */
internal suspend fun runPeriodically(
        num: Int,
        timeUnit: TimeUnit = TimeUnit.SECONDS,
        task: suspend () -> Unit
) {
    val period = when (timeUnit) {
        TimeUnit.MINUTES -> (num * 10_000).toLong()
        TimeUnit.HOURS -> (num * 100_000).toLong()
        else -> (num * 1000).toLong()
    }
    while (true) {
        task()
        delay(period)
    }
}