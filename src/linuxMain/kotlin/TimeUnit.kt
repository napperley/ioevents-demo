package org.example.ioevents_demo

internal enum class TimeUnit {
    SECONDS,
    MINUTES,
    HOURS
}