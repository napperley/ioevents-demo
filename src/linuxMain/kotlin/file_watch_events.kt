package org.example.ioevents_demo

import platform.linux.*

internal enum class FileWatchEvent(val mask: Int) {
    ACCESS(IN_ACCESS),
    CREATE(IN_CREATE),
    DELETE(IN_DELETE),
    SELF_DELETE(IN_DELETE_SELF),
    MODIFY(IN_MODIFY),
    SELF_MOVE(IN_MOVE_SELF),
    MOVE_FROM(IN_MOVED_FROM),
    MOVE_TO(IN_MOVED_TO),
    OPEN(IN_OPEN),
    ATTRIB_CHANGE(IN_ATTRIB),
    WRITE_CLOSE(IN_CLOSE_WRITE),
    NO_WRITE_CLOSE(IN_CLOSE_NOWRITE)
}

internal fun createFileWatchEventMask(vararg events: FileWatchEvent): Int {
    var result = 0
    events.forEach { evt ->
        result = if (result == 0) evt.mask
        else result or evt.mask
    }
    return result
}
