group = "org.example"
version = "0.1-SNAPSHOT"

plugins {
    kotlin("multiplatform") version "1.3.50"
}

repositories {
    mavenCentral()
    jcenter()
}

kotlin {
    linuxX64("linux") {
        compilations.getByName("main") {
            dependencies {
                val kotlinxCoroutinesVer = "1.3.0"
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core-native:$kotlinxCoroutinesVer")
            }
        }
        binaries {
            executable("ioevents_demo") {
                entryPoint = "org.example.ioevents_demo.main"
            }
        }
    }
}